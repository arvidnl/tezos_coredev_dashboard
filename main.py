import argparse
import action_collector
import action_printer
import projects
import xgitlab


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('--private-token', type=str, required=True)
    parser.add_argument(
        '--output-dir',
        type=str,
        default=".",
    )
    return parser.parse_args()


def main(args) -> None:
    server = xgitlab.Server.default(
        projects=projects.PROJECTS,
        private_token=args.private_token,
        dashboard_maintainers=projects.DASHBOARD_MAINTAINERS,
    )
    server.resolve_deps()
    actions = list(action_collector.collect_actions_from_server(server))
    printer = action_printer.Printer()
    printer.print_all(server, actions, output_dir=args.output_dir)


if __name__ == '__main__':
    main(parse_arguments())
