from abc import ABC
from functools import cached_property
from typing import (
    Any,
    Callable,
    Dict,
    Iterable,
    List,
    Literal,
    NamedTuple,
    Optional,
    Sequence,
    Set,
    Tuple,
    Type,
    TypeVar,
    Union,
)
from datetime import datetime, timedelta
from os import path
from gitlab import Gitlab  # type: ignore
from utils import GitlabDatetime, Wrapper
import itertools
import re
import unicodedata
import os
import random
import gitlab  # type: ignore
import gitlab.v4.objects as gitlab_objects  # type: ignore
import projects_spec
import cache

# Parsers and textual heuristics

ResourceKind = Literal['!', '#']
Dependency = Tuple[str, ResourceKind, int]

RE_MENTION = re.compile(r'@(\w(?:[-.\w]|\\[-._])*)')
RE_PUSH = re.compile(r'added \d+ commit')
RE_BLOCKERS = re.compile(
    r'^(?:blocked|depends?|.*\bdepends\s+on|.*\bbased\s+on)\b(.*)$',
    re.MULTILINE + re.IGNORECASE,
)
RE_DEPENDENCY = re.compile(
    (
        r'(?P<uri>(?:https?://)?(?:www\.)?gitlab\.com/)?((?(uri)\w[-\w/]*'
        r'|(?:\b\w[-\w]*/\w[-\w/]*)?))((?(uri)(?:issues|merge_requests)'
        r'|[!#]))(?(uri)/)(\d+)(?:\b|(?(uri)#\w*|\b))'
    )
)
RE_ISSUE = re.compile(r'^([^#]+)#(\d+)$')
RE_WIP = re.compile(r'^WIP\b', re.IGNORECASE)
RE_META = re.compile(
    r'^(?:\[?(?:meta[- ]issue\b|overview\b|☂))|(?:.*\bmeta[- ]issue$)',
    re.IGNORECASE,
)
RE_NON_FILENAME = re.compile(r'[^\w.-]')
RE_NON_FILEPATH = re.compile(fr'[^\w.{re.escape(os.sep)}-]')


def is_automatic_merge(text: str) -> bool:
    return text.startswith('enabled an automatic merge')


def get_blockers_sections(text: str) -> List[str]:
    return RE_BLOCKERS.findall(text)


def parse_dependency(
    raw_dep: Tuple[
        str, str, Literal['!', '#', 'issues', 'merge_requests'], str
    ]
) -> Dependency:
    server, repo, kind, id = raw_dep
    if repo.endswith('/-/'):
        repo = repo[:-3]
    elif repo.endswith('/'):
        repo = repo[:-1]
    if kind == 'issues':
        kind = '#'
    elif kind == 'merge_requests':
        kind = '!'
    return (repo, kind, int(id))


def get_dependencies(text: str) -> List[Dependency]:
    return [parse_dependency(d) for d in RE_DEPENDENCY.findall(text)]


def is_changed_line(text: str) -> bool:
    return text.startswith('changed this line')


def is_push(text: str) -> bool:
    return RE_PUSH.match(text) is not None


def normalize_mention(name: str) -> str:
    return name.replace('\\', '')


def get_mentioned_users(text: str) -> Set[str]:
    return set(normalize_mention(u) for u in RE_MENTION.findall(text))


def get_reviewers_section(text: str) -> str:
    i = text.lower().rfind('reviewer')
    return text[i:] if i >= 0 else ''


def parse_issue_ref(ref: str) -> Tuple[str, int]:
    m = RE_ISSUE.match(ref)
    assert m is not None
    repo, id = m.groups()
    return (repo, int(id))


def title_is_wip(title: str) -> bool:
    return RE_WIP.match(title) is not None


def title_is_meta(title: str) -> bool:
    return RE_META.match(title) is not None


def to_ascii(s: str) -> str:
    return (
        unicodedata.normalize('NFKD', s)
        .encode('ascii', 'ignore')
        .decode('ascii')
    )


def to_filename(s: str) -> str:
    return RE_NON_FILENAME.sub('*', to_ascii(s)).strip('*').replace('*', '_')


def to_filepath(s: str) -> str:
    return RE_NON_FILEPATH.sub('*', to_ascii(s)).strip('*').replace('*', '_')


# Some types

TThreadWeight = int
TCommentWeight = Tuple[float, TThreadWeight]
TRepoPreWeight = int
TLabelWeight = int
TResourceLabelWeights = List[TLabelWeight]
TResourceReadyWeight = int
TResourceUserWeight = bool
TResourceVotesWeight = int
TResourceAgeWeight = float
TResourceBlockingWeight = int
TResourceSelfWeight = Tuple[
    TRepoPreWeight,
    TResourceLabelWeights,
    TResourceBlockingWeight,
    TResourceReadyWeight,
    TResourceUserWeight,
]
TResourcePreWeight = Tuple[
    TRepoPreWeight,
    TResourceLabelWeights,
    TResourceSelfWeight,
    TResourceBlockingWeight,
    TResourceReadyWeight,
]
TResourcePostWeight = Tuple[
    TResourceUserWeight, TResourceVotesWeight, TResourceAgeWeight
]


# Classes


class User:
    avatar_url = None

    def __init__(
        self,
        server: 'Server',
        username: str,
    ):
        self.username = username
        self.name = username
        self.web_url = f'https://gitlab.com/{username}'
        self.server = server

    def update_from_dict(self, dic: dict) -> 'User':
        self.name = dic['name']
        self.avatar_url = dic['avatar_url']
        self.web_url = dic['web_url']
        return self

    def update_from_project_member(
        self, mem: gitlab_objects.ProjectMember
    ) -> 'User':
        self.name = mem.name
        self.avatar_url = mem.avatar_url
        self.web_url = mem.web_url
        return self

    def __lt__(self, other: 'User') -> bool:
        return self.username < other.username

    def __eq__(self, other: object) -> bool:
        return isinstance(other, User) and self.username == other.username

    def __hash__(self) -> int:
        return hash(self.username)

    @cached_property
    def printable(self) -> str:
        return self.username

    @cached_property
    def printable_name(self) -> str:
        return getattr(self, 'name', self.printable)

    @cached_property
    def avatar_alt(self) -> str:
        return f'@{self.username}'

    def __repr__(self) -> str:
        return f'{type(self).__name__}({self.username.__repr__()})'

    @cached_property
    def is_dashboard_maintainer(self) -> bool:
        return self in self.server.dashboard_maintainers

    @cached_property
    def filename(self) -> str:
        return to_filename(self.username)


TTempUser = TypeVar('TTempUser', bound='TempUser')


class TempUser(User):
    def __init__(self, username: str):
        self.username = username

    @classmethod
    def from_dict(cls: Type[TTempUser], dic: dict) -> TTempUser:
        return cls(dic['username'])


class Push(Wrapper):
    def __init__(self, wrapped: Union['Comment', 'MergeRequest']):
        super().__init__(wrapped)

    def __repr__(self) -> str:
        return f'{type(self).__name__}({self.wrapped.__repr__()})'

    @cached_property
    def is_mere_rebase(self) -> bool:
        # TODO
        # We could detect this by comparing the commit number / titles
        # Also for repositories requiring signed commits, automated rebases
        # are not signed
        return False


class Comment:
    def __init__(self, thread: 'Thread', note_dict: dict):
        self.thread = thread
        self.__attributes = note_dict

    @cached_property
    def uid(self) -> int:
        return self.__attributes['id']

    @cached_property
    def author(self) -> User:
        return self.thread.mr.repo.server.user_from_dict(
            self.__attributes['author']
        )

    @cached_property
    def system(self) -> bool:
        return self.__attributes['system']

    @cached_property
    def body(self) -> str:
        return self.__attributes['body']

    @cached_property
    def created_at(self) -> datetime:
        return GitlabDatetime.parse(self.__attributes['created_at'])

    @cached_property
    def changed_line(self) -> bool:
        return self.system and is_changed_line(self.body)

    @cached_property
    def automatic_merger(self) -> Optional[User]:
        return (
            self.author
            if self.system and is_automatic_merge(self.body)
            else None
        )

    @cached_property
    def as_push(self) -> Optional[Push]:
        return Push(self) if self.system and is_push(self.body) else None

    @cached_property
    def web_url(self) -> str:
        return f"{self.thread.mr.web_url}#note_{self.__attributes['id']}"

    def __repr__(self) -> str:
        rep = self.thread.__repr__()
        return f'{type(self).__name__}({self.uid.__repr__()} on {rep})'

    @cached_property
    def weight(self) -> TCommentWeight:
        return (-self.created_at.timestamp(), self.thread.weight)


class Thread(Wrapper):
    def __init__(
        self,
        mr: 'MergeRequest',
        discussion: gitlab_objects.ProjectMergeRequestDiscussion,
    ):
        super().__init__(discussion)
        self.mr = mr
        self.__notes = discussion.attributes['notes']

    @cached_property
    def unresolved_notes(self) -> List[dict]:
        return [
            n
            for n in self.__notes[::-1]
            if n['resolvable'] and not n['resolved']
        ]

    @cached_property
    def last_non_author_unresolved_comment(self) -> Optional[Comment]:
        for n in self.unresolved_notes:
            if TempUser.from_dict(n['author']) not in self.mr.authors:
                return Comment(self, n)
        return None

    @cached_property
    def unresolved_comments(self) -> Dict[User, Comment]:
        return {
            self.mr.repo.server.user_from_dict(n['author']): Comment(self, n)
            for n in self.unresolved_notes[::-1]
        }

    @cached_property
    def resolved(self) -> bool:
        return not any(self.unresolved_notes)

    @cached_property
    def first_comment(self) -> Comment:
        return Comment(self, self.__notes[0])

    @cached_property
    def last_comment(self) -> Comment:
        return Comment(self, self.__notes[-1])

    @cached_property
    def last_comment_is_from_author(self) -> bool:
        return self.last_comment.author in self.mr.authors

    @cached_property
    def line_has_been_updated(self) -> bool:
        return self.last_comment.changed_line

    @cached_property
    def last_comment_is_from_author_or_line_has_been_updated(self) -> bool:
        return self.last_comment_is_from_author or self.line_has_been_updated

    @cached_property
    def automatic_merger(self) -> Optional[User]:
        return (
            self.last_comment.automatic_merger
            if self.individual_note
            else None
        )

    @cached_property
    def as_push(self) -> Optional[Push]:
        return self.last_comment.as_push if self.individual_note else None

    def __repr__(self) -> str:
        mr_rep = self.mr.__repr__()
        return f'{type(self).__name__}({self.id.__repr__()} on {mr_rep})'

    @cached_property
    def weight(self) -> TThreadWeight:
        return len(self.__notes)


class Label(ABC):
    priority: Optional[int]

    def __init__(
        self,
        repo: 'Repository',
        name: str,
    ):
        self.repo = repo
        self.name = name

    @cached_property
    def filename(self) -> str:
        return to_filename(self.name)

    @cached_property
    def is_meta(self) -> bool:
        return self.name in ['Meta', 'meta', 'meta-issue']

    @cached_property
    def weight(self) -> TLabelWeight:
        if self.priority is not None:
            return 9999 - self.priority
        return self.repo.default_label_weight


class UnknownLabel(Label):
    color = 'white'
    text_color = 'black'
    priority = None
    description = ''
    printable_weight = 'Unknown'


class ProjectLabel(Wrapper, Label):
    def __init__(self, repo: 'Repository', label: gitlab_objects.ProjectLabel):
        Wrapper.__init__(self, label)
        Label.__init__(self, repo, label.name)
        self.color = label.color
        self.text_color = label.text_color
        self.description = (
            label.description if label.description is not None else ''
        )
        self.printable_weight = (
            f'{label.priority}'
            if label.priority is not None
            else 'Not prioritized'
        )


class Resource(Wrapper):
    resource_char: str
    filepath_element: str

    def __init__(
        self,
        repo: 'Repository',
        resource: Union[
            gitlab_objects.ProjectMergeRequest,
            gitlab_objects.ProjectIssue,
            gitlab_objects.Issue,
        ],
    ):
        super().__init__(resource)
        self.repo = repo
        self.blocking: Set['Resource'] = set()
        self.internal_blocking_rec: Optional[Set['Resource']] = None
        self.internal_self_weight_rec: Optional[TResourceSelfWeight] = None

    @cached_property
    def uid(self) -> str:
        return self.id

    @cached_property
    def printable_id(self) -> str:
        return f'{self.repo.printable_id}{self.resource_char}{self.iid}'
        # self.references['full'] is not populated for simple-viewed resources

    def __lt__(self, other: 'Resource') -> bool:
        return self.printable_id < other.printable_id

    def __eq__(self, other: object) -> bool:
        return (
            isinstance(other, Resource)
            and self.printable_id == other.printable_id
        )

    def __hash__(self) -> int:
        return hash(self.uid)

    @cached_property
    def real_title(self) -> str:
        return self.get_wrapped_attr('title')

    @cached_property
    def title(self) -> str:
        return self.real_title if self.visible else self.printable_id

    def __repr__(self) -> str:
        return f'{type(self).__name__}({self.printable_id.__repr__()})'

    @cached_property
    def created_at(self) -> datetime:
        return GitlabDatetime.parse(self.get_wrapped_attr('created_at'))

    def mark_as_blocking(self, other: 'Resource') -> None:
        if other != self:
            self.blocking.add(other)

    @cached_property
    def description(self) -> str:
        return self.get_wrapped_attr('description') or ''

    def _get_blockers(self) -> Set['Resource']:
        return {
            d
            for d in (
                self.repo.get_open_dependency(dep)
                for section in get_blockers_sections(self.description)
                for dep in (get_dependencies(section))
            )
            if d is not None
        }

    def resolve_deps(self) -> None:
        # TODO: currently no API to get dependent MRs/issues
        blockers = self._get_blockers()
        for b in blockers:
            b.mark_as_blocking(self)
        self.blockers = blockers

    @cached_property
    def blocked(self) -> bool:
        return any(self.blockers)

    @cached_property
    def printable_blockers(self) -> str:
        return ', '.join([blocker.printable_id for blocker in self.blockers])

    def get_blocking_rec(self, max_rec: int) -> Set['Resource']:
        if self.internal_blocking_rec is None:
            if max_rec <= 0:
                self.internal_blocking_rec = self.blocking
            else:
                self.internal_blocking_rec = set.union(
                    *(
                        dep.get_blocking_rec(max_rec - 1)
                        for dep in self.blocking
                    ),
                    self.blocking,
                )
        return self.internal_blocking_rec

    @cached_property
    def blocking_rec(self) -> Set['Resource']:
        return self.get_blocking_rec(50)

    @cached_property
    def blocking_weight(self) -> TResourceBlockingWeight:
        return 1 + len(self.blocking_rec)

    @cached_property
    def votes_weight(self) -> TResourceVotesWeight:
        return self.upvotes - self.downvotes

    @cached_property
    def age_weight(self) -> TResourceAgeWeight:
        return -self.created_at.timestamp()

    @cached_property
    def self_self_weight(self) -> TResourceSelfWeight:
        return (
            self.repo.pre_weight,
            self.labels_weights,
            self.blocking_weight,
            self.ready_weight,
            self.user_weight,
        )

    def get_self_weight_rec(self, max_rec: int) -> TResourceSelfWeight:
        if self.internal_self_weight_rec is None:
            self.internal_self_weight_rec = self.self_self_weight
            if max_rec > 0:
                for dep in self.blocking:
                    self.internal_self_weight_rec = max(
                        self.internal_self_weight_rec,
                        dep.get_self_weight_rec(max_rec - 1),
                    )
        return self.internal_self_weight_rec

    @cached_property
    def self_weight(self) -> TResourceSelfWeight:
        return self.get_self_weight_rec(50)

    @cached_property
    def pre_weight(self) -> TResourcePreWeight:
        return (
            self.repo.pre_weight,
            self.labels_weights,
            self.self_weight,
            self.blocking_weight,
            self.ready_weight,
        )

    @cached_property
    def post_weight(self) -> TResourcePostWeight:
        return (
            self.user_weight,
            self.votes_weight,
            self.age_weight,
        )

    @cached_property
    def labels(self) -> Set[Label]:
        return set(
            self.repo.get_label(lbl) for lbl in self.get_wrapped_attr('labels')
        )

    @cached_property
    def labels_weights(self) -> TResourceLabelWeights:
        keep = 5
        result = sorted((lbl.weight for lbl in self.labels), reverse=True)[
            :keep
        ]
        result += [self.repo.default_label_weight] * (keep - len(result))
        return result

    @cached_property
    def tcs(self) -> Tuple[int, int]:
        res = self.get_wrapped_attr('task_completion_status')
        return (res['completed_count'], res['count'])

    @cached_property
    def has_uncompleted_tasks(self) -> bool:
        return self.tcs[0] < self.tcs[1]

    @cached_property
    def task_completion_uid(self) -> str:
        return f"{self.tcs[0]/self.tcs[1]}"

    @cached_property
    def task_completion_weight(self) -> int:
        return self.tcs[1] - self.tcs[0]

    @cached_property
    def assignees(self) -> Set[User]:
        return {
            self.repo.server.user_from_dict(u)
            for u in self.get_wrapped_attr('assignees')
        }

    @cached_property
    def updated_at(self) -> datetime:
        return GitlabDatetime.parse(self.get_wrapped_attr('updated_at'))

    @cached_property
    def filepath(self) -> str:
        return path.join(
            self.repo.filepath, self.filepath_element, str(self.iid)
        )


class MergeRequest(Resource):
    resource_char = '!'
    filepath_element = 'mergerequests'

    def __init__(
        self, repo: 'Repository', mr: gitlab_objects.ProjectMergeRequest
    ):
        super().__init__(repo, mr)

    @cached_property
    def wrapped_more(self) -> gitlab_objects.ProjectMergeRequest:
        return cache.get_one(
            gitlab_objects.ProjectMergeRequest,
            f'MR {self.printable_id}',
            self.repo,
            ['mergerequests'],
            args=[self.iid],
            keywords={
                'include_diverged_commits_count': True,
                'include_rebase_in_progress': True,
            },
            after=self.updated_at,
        )

    @cached_property
    def author(self) -> User:
        return self.repo.server.user_from_dict(self.get_wrapped_attr('author'))

    @cached_property
    def pushes(self) -> Sequence[Push]:
        first_push = Push(self)
        thread_pushes = [
            t.as_push for t in self.threads if t.as_push is not None
        ]
        return [first_push] + thread_pushes

    @cached_property
    def last_push(self) -> datetime:
        if any(self.pushes):
            return self.pushes[-1].created_at
        return self.created_at

    @cached_property
    def last_push_by_original_author(self) -> datetime:
        for p in self.pushes[::-1]:
            if p.is_mere_rebase:
                continue
            if p.author == self.author:
                return p.created_at
        return self.created_at

    @cached_property
    def authors(self) -> Dict[User, str]:
        earliest_authoring_date = max(
            self.last_push - timedelta(days=60),
            self.last_push_by_original_author + timedelta(days=10),
        )
        authors = {
            p.author: 'pushed recently'
            for p in self.pushes
            if not p.is_mere_rebase and p.created_at > earliest_authoring_date
        }
        if self.author in authors or not any(authors):
            authors[self.author] = 'is the original author'
        return authors

    @cached_property
    def merger(self) -> Optional[Tuple[User, str]]:
        if self.merged_by is not None:
            return (
                self.repo.server.user_from_dict(self.merged_by),
                'merged',
            )
        for t in self.threads[::-1]:
            if t.automatic_merger:
                return (t.automatic_merger, 'set automatic merge')
        return None

    @cached_property
    def authors_and_merger(self) -> Dict[User, str]:
        res = self.authors
        if not self.merger:
            return res
        merger_user, merger_why = self.merger
        if merger_user in res:
            merger_why = f'{res[merger_user]} and {merger_why}'
        return {**res, merger_user: merger_why}

    @cached_property
    def authors_and_pipeline_user(self) -> Dict[User, str]:
        res = self.authors
        head_pipeline_user = self.repo.server.user_from_dict(
            self.head_pipeline['user']
        )
        head_pipeline_why = 'triggered last pipeline'
        if head_pipeline_user in res:
            head_pipeline_why = (
                f'{res[head_pipeline_user]} and {head_pipeline_why}'
            )
        return {**res, head_pipeline_user: head_pipeline_why}

    @cached_property
    def reviewers_in_description(self) -> Set[User]:
        return {
            self.repo.server.user_from_username(un)
            for un in get_mentioned_users(
                get_reviewers_section(self.description)
            )
        }

    @cached_property
    def _approvals(self) -> gitlab_objects.ProjectMergeRequestApproval:
        return cache.get_one(
            gitlab_objects.ProjectMergeRequestApproval,
            f'approvals for MR {self.printable_id}',
            self,
            ['approvals'],
            after=self.updated_at,
        )

    @cached_property
    def approvers(self) -> Set[User]:
        return {
            self.repo.server.user_from_dict(a['user'])
            for a in self._approvals.approved_by
        }

    @cached_property
    def set_reviewers(self) -> Set[User]:
        return {
            self.repo.server.user_from_dict(u)
            for u in self.get_wrapped_attr('reviewers')
        }

    @cached_property
    def all_reviewers(self) -> Set[User]:
        return (
            self.set_reviewers
            | self.reviewers_in_description
            | self.assignees
            | self.approvers
        )

    @cached_property
    def _discussions(
        self,
    ) -> List[gitlab_objects.ProjectMergeRequestDiscussion]:
        return cache.get_list(
            gitlab_objects.ProjectMergeRequestDiscussion,
            f'threads for MR {self.printable_id}',
            self,
            ['discussions'],
            keywords={'all': True},
            after=self.updated_at,
        )

    @cached_property
    def threads(self) -> Sequence[Thread]:
        return [Thread(self, d) for d in self._discussions]

    @cached_property
    def unresolved_threads(self) -> List[Thread]:
        return [t for t in self.threads if not t.resolved]

    @cached_property
    def has_unresolved_threads(self) -> bool:
        return any(self.unresolved_threads)

    @cached_property
    def one_approval_left(self) -> bool:
        return self._approvals.approvals_left == 1

    @cached_property
    def has_at_least_one_approval(self) -> bool:
        return any(self._approvals.approved_by)

    @cached_property
    def has_diverged(self) -> bool:
        return self.diverged_commits_count > 0

    @cached_property
    def non_approvers_reviewers(self) -> Dict[User, str]:
        return {
            **{
                u: 'is mentioned as reviewer'
                for u in self.reviewers_in_description - self.approvers
            },
            **{u: 'is assigned' for u in self.assignees - self.approvers},
            **{
                u: 'is set as reviewer'
                for u in self.set_reviewers - self.approvers
            },
        }

    @cached_property
    def mergeteam_reviewers(self) -> Dict[User, str]:
        return {
            u: f'is a merge-teamer and {w}'
            for u, w in self.non_approvers_reviewers.items()
            if u in self.repo.mergeteamers
        }

    @cached_property
    def mergers(self) -> Dict[User, str]:
        return (
            self.mergeteam_reviewers
            if self.mergeteam_reviewers
            else {
                u: f'is a dispatcher and there is no merge-team reviewer'
                for u in self.repo.dispatchers
            }
        )

    @cached_property
    def has_less_than_2_reviewers(self) -> bool:
        return len(self.all_reviewers) < 2

    @cached_property
    def has_no_non_dispatcher_mergeteamers_reviewers(self) -> bool:
        return not any(self.mergeteam_reviewers.keys() - self.repo.dispatchers)

    @cached_property
    def last_pipeline_status(self) -> Optional[str]:
        return (
            self.head_pipeline['status']
            if self.head_pipeline is not None
            else None
        )

    @cached_property
    def last_pipeline_failed(self) -> bool:
        return self.last_pipeline_status == 'failed'

    @cached_property
    def last_pipeline_succeeded(self) -> bool:
        return (
            self.last_pipeline_status is None
            or self.last_pipeline_status == 'success'
        )

    @cached_property
    def pipelines(
        self,
    ) -> Iterable[gitlab_objects.ProjectMergeRequestPipeline]:
        return self.get_wrapped_attr('pipelines').list(all=True)

    @cached_property
    def no_pipeline_finished(self) -> bool:
        for pipeline in self.pipelines:
            if pipeline.status != 'running':
                return False
        return True

    @cached_property
    def last_finished_pipeline_succeeded(self) -> bool:
        if self.last_pipeline_succeeded:
            return True
        for pipeline in self.pipelines:
            if pipeline.status == 'success':
                return True
            if pipeline.status == 'failed':
                return False
        return False

    @cached_property
    def base_uid(self) -> str:
        return self.diff_refs['base_sha']

    @cached_property
    def wip(self) -> bool:
        return self.work_in_progress or title_is_wip(self.real_title)

    @cached_property
    def ready_weight(self) -> TResourceReadyWeight:
        return -self.wip

    @cached_property
    def user_weight(self) -> TResourceUserWeight:
        return self.first_contribution

    @cached_property
    def visible(self) -> bool:
        return self.repo.merge_requests_visible

    @cached_property
    def merge_when_pipeline_succeeds(self) -> bool:
        try:
            return self.get_wrapped_attr('merge_when_pipeline_succeeds')
        except gitlab.exceptions.GitlabGetError as exc:
            return False


class Issue(Resource):
    is_special = False
    resource_char = '#'
    filepath_element = 'issues'

    def __init__(
        self,
        repo: 'Repository',
        issue: Union[gitlab_objects.Issue, gitlab_objects.ProjectIssue],
    ):
        super().__init__(repo, issue)

    @cached_property
    def _closed_by(self) -> List[Dict[str, Any]]:
        return cache.fetch(
            f'MRs closed by {self.printable_id}',
            path=[self.filepath, 'closed_by'],
            f=self.closed_by,
            keywords={'all': True},
            after=self.updated_at,
        )

    def _get_blockers(self) -> Set[Resource]:
        dep_mrs = {
            mr
            for mr in (
                self.repo.server.get_repo(mr['project_id']).get_open_mr(
                    mr['iid']
                )
                for mr in self._closed_by
                if mr['state'] == 'opened'
            )
            if mr is not None
        }
        return super()._get_blockers() | dep_mrs

    @cached_property
    def author(self) -> User:
        return self.repo.server.user_from_dict(self.get_wrapped_attr('author'))

    @cached_property
    def is_assigned(self) -> bool:
        return any(self.assignees)

    @cached_property
    def owners(self) -> Dict[User, str]:
        return (
            {u: 'is assigned' for u in self.assignees}
            if any(self.assignees)
            else {self.author: 'opened issue and no one is assigned'}
        )

    @cached_property
    def visible(self) -> bool:
        return self.repo.server.allow_private or (
            self.repo.issues_visible and not self.confidential
        )

    @cached_property
    def is_meta(self) -> bool:
        for lbl in self.labels:
            if lbl.is_meta:
                return True
        return title_is_meta(self.real_title)

    @cached_property
    def ready_weight(self) -> TResourceReadyWeight:
        return -self.is_meta

    @cached_property
    def user_weight(self) -> TResourceUserWeight:
        return False  # TODO


class Repository(Wrapper):
    def __init__(
        self,
        server: 'Server',
        project: Union[gitlab_objects.Project, gitlab_objects.GroupProject],
    ):
        super().__init__(project)
        self.server = server
        self.registered = False
        self.config = projects_spec.EMPTY_PROJECT_CONFIG
        self.__issues: Dict[int, Issue] = dict()
        self.__labels: Dict[str, Label] = dict()

    @cached_property
    def wrapped_more(self) -> gitlab_objects.Project:
        return self.server.fetch_raw_project(self.id)

    @cached_property
    def direct_members(self) -> List[gitlab_objects.ProjectMember]:
        return list(self.members.list(all=True))

    @cached_property
    def all_members(self) -> List[gitlab_objects.ProjectMember]:
        print(f'Fetching members for {self.printable_id}', flush=True)
        try:
            return self.members_all.list(all=True)
        except gitlab.exceptions.GitlabListError as exc:
            print(
                f'  Failure (will use direct members only): {exc}', flush=True
            )
            # TODO: also get parent group members
            # There's a problem with the members_all endpoint that returns an
            # error 500
            return self.direct_members

    @cached_property
    def owners(self) -> Set[User]:
        if not any(self.all_members):
            return set()
        highest_level = max(
            self.all_members, key=lambda m: m.access_level
        ).access_level
        return {
            self.server.user_from_project_member(m)
            for m in self.all_members
            if m.access_level == highest_level
        }

    @cached_property
    def dispatchers_and_why(self) -> Tuple[Set[User], str]:
        return self.server.users_from_people_spec(
            self.config.dispatchers,
            repo=self,
            default=(self.owners, 'from project owners'),
        )

    @cached_property
    def dispatchers(self) -> Set[User]:
        return self.dispatchers_and_why[0]

    @cached_property
    def dispatchers_why(self) -> str:
        return self.dispatchers_and_why[1]

    @cached_property
    def dashboard_maintainers_or_dispatchers(self) -> Set[User]:
        return (
            self.server.dashboard_maintainers
            if any(self.server.dashboard_maintainers)
            else self.dispatchers
        )

    @cached_property
    def issuewatchers_and_why(self) -> Tuple[Set[User], str]:
        return self.server.users_from_people_spec(
            self.config.issuewatchers,
            repo=self,
            default=(set(), 'no one'),
        )

    @cached_property
    def issuewatchers(self) -> Set[User]:
        return self.issuewatchers_and_why[0]

    @cached_property
    def issuewatchers_why(self) -> str:
        return self.issuewatchers_and_why[1]

    @cached_property
    def required_approvers(self) -> Set[User]:
        try:
            rules = cache.get_list(
                gitlab_objects.ProjectApprovalRule,
                f'approval rules for project {self.printable_id}',
                self,
                ['approvalrules'],
                keywords={'all': True},
                max_age=timedelta(hours=2, minutes=random.randint(0, 60)),
            )
        except gitlab.exceptions.GitlabListError as exc:
            print(
                (
                    'Could not list approval rules for '
                    f'{self.printable_id}: {exc}'
                ),
                flush=True,
            )
            rules = []

        return (
            {
                self.server.user_from_dict(user)
                for rule in rules
                if rule.approvals_required > 0
                for user in rule.eligible_approvers
            }
            if self.merge_requests_enabled
            else set()
        )

    @cached_property
    def mergeteamers_and_why(self) -> Tuple[Set[User], str]:
        return self.server.users_from_people_spec(
            self.config.mergeteamers,
            repo=self,
            default=(self.required_approvers, 'from approval rules')
            if any(self.required_approvers)
            else (self.dispatchers, 'copied from dispatchers'),
        )

    @cached_property
    def mergeteamers(self) -> Set[User]:
        return self.mergeteamers_and_why[0]

    @cached_property
    def mergeteamers_why(self) -> str:
        return self.mergeteamers_and_why[1]

    @cached_property
    def printable_dispatchers(self) -> str:
        return ', '.join(sorted([d.username for d in self.dispatchers]))

    @cached_property
    def opened_mrs(self) -> List[MergeRequest]:
        print(f'Listing opened MRs for {self.printable_id}', flush=True)
        return (
            [
                MergeRequest(self, mr)
                for mr in self.mergerequests.list(
                    state='opened', all=True, view='simple'
                )
            ]
            if self.merge_requests_enabled
            else []
        )

    def get_issue(self, iid: int) -> Issue:
        res = self.__issues.get(iid)
        if res is None:
            raw_issue = cache.get_one(
                gitlab_objects.Issue,
                f'issue {iid} on {self.printable_id}',
                self,
                ['issues'],
                args=[iid],
            )
            res = self.__add_raw_issue(raw_issue)
        return res

    def __add_raw_issue(self, raw_issue: gitlab_objects.Issue) -> Issue:
        res = Issue(self, raw_issue)
        self.__issues[res.iid] = res
        return res

    @cached_property
    def opened_issues(self) -> List[Issue]:
        if not self.issues_enabled:
            print(
                f'Issues are not enabled for {self.printable_id}', flush=True
            )
            return []
        print(f'Listing opened issues for {self.printable_id}', flush=True)
        return [
            self.__add_raw_issue(
                cache.add(self, ['issues', raw_issue.iid], raw_issue)
            )
            for raw_issue in self.issues.list(state='opened', all=True)
        ]

    def get_label(self, name: str) -> Label:
        _ = self.__load_labels
        res = self.__labels.get(name)
        if res is None:
            res = UnknownLabel(self, name)
            self.__labels[name] = res
        return res

    @cached_property
    def __load_labels(self) -> None:
        labels = cache.get_list(
            gitlab_objects.ProjectLabel,
            f'labels for project {self.printable_id}',
            self,
            ['labels'],
            keywords={'all': True},
            max_age=timedelta(hours=2, minutes=random.randint(0, 60)),
        )
        for lbl in labels:
            self.__labels[lbl.name] = ProjectLabel(self, lbl)

    @cached_property
    def all_labels(self) -> Set[Label]:
        _ = self.__load_labels
        return set(self.__labels.values())

    @cached_property
    def default_label(self) -> Optional[Label]:
        if self.config.default_label is not None:
            return self.get_label(self.config.default_label)
        return None

    @cached_property
    def default_label_weight(self) -> TLabelWeight:
        if self.default_label is not None:
            return self.default_label.weight
        return 0

    @cached_property
    def printable_id(self) -> str:
        return self.path_with_namespace

    @cached_property
    def visible(self) -> bool:
        return self.server.allow_private or self.visibility == 'public'

    @cached_property
    def issues_visible(self) -> bool:
        return self.server.allow_private or (
            self.visible and self.issues_access_level == 'enabled'
        )

    @cached_property
    def merge_requests_visible(self) -> bool:
        return self.server.allow_private or (
            self.visible and self.merge_requests_access_level == 'enabled'
        )

    def register_config(self, config: projects_spec.ProjectConfig) -> None:
        self.registered = True
        self.config = config.inherited_from(self.config)

    def __repr__(self) -> str:
        return f'{type(self).__name__}({self.printable_id.__repr__()})'

    @cached_property
    def avatar_alt(self) -> str:
        return self.printable_id

    @cached_property
    def pre_weight(self) -> TRepoPreWeight:
        return (
            self.config.pre_weight
            if self.config.pre_weight is not None
            else -1
        )

    @cached_property
    def filepath(self) -> str:
        return to_filepath(self.printable_id)

    def get_open_mr(self, id: int) -> Optional[MergeRequest]:
        return next((mr for mr in self.opened_mrs if mr.iid == id), None)

    def get_open_issue(self, id: int) -> Optional[Issue]:
        return next(
            (issue for issue in self.opened_issues if issue.iid == id),
            None,
        )

    def get_open_resource(
        self, kind: ResourceKind, id: int
    ) -> Optional[Resource]:
        if kind == '!':
            return self.get_open_mr(id)
        else:
            return self.get_open_issue(id)

    def get_open_dependency(self, dep_ref: Dependency) -> Optional[Resource]:
        if dep_ref[0] == '' or dep_ref[0] == self.printable_id:
            repo = self
        else:
            repo = self.server.get_repo(dep_ref[0])
        return repo.get_open_resource(dep_ref[1], dep_ref[2])

    def resolve_deps(self) -> None:
        for rc in itertools.chain(self.opened_mrs, self.opened_issues):
            rc.resolve_deps()


class Group(Wrapper):
    def __init__(
        self,
        server: 'Server',
        group: Union[gitlab_objects.Group, gitlab_objects.GroupSubgroup],
    ):
        super().__init__(group)
        self.server = server

    @cached_property
    def repos(self) -> List[Repository]:
        print(f'Listing group projects for {self.id}', flush=True)
        return [
            self.server.get_or_add_raw_repo(p)
            for p in self.projects.list(all=True, include_subgroups=True)
        ]

    def register_config(self, config: projects_spec.ProjectConfig) -> None:
        for p in self.repos:
            p.register_config(config)

    def __repr__(self) -> str:
        return f'{type(self).__name__}({self.full_path.__repr__()})'


TServer = TypeVar('TServer', bound='Server')


class Server(Wrapper):
    def __init__(
        self,
        gitlab: Gitlab,
        allow_private: bool = False,
        dashboard_maintainers_spec: projects_spec.PeopleSpec = projects_spec.NO_ONE,  # noqa: ignore line length here, black bug
    ):
        super().__init__(gitlab)
        self.__group_by_name: Dict[str, Group] = dict()
        self.__group_by_id: Dict[int, Group] = dict()
        self.__repo_by_name: Dict[str, Repository] = dict()
        self.__repo_by_id: Dict[int, Repository] = dict()
        self.users: Dict[str, User] = dict()
        self.allow_private = allow_private
        self.dashboard_maintainers_spec = dashboard_maintainers_spec

    @classmethod
    def default(
        cls: Type[TServer],
        *,
        projects: projects_spec.ProjectRef = projects_spec.EMPTY_PROJECT_SPEC,
        private_token: Optional[str] = None,
        allow_private: bool = False,
        dashboard_maintainers: projects_spec.PeopleSpec = projects_spec.NO_ONE,
    ) -> TServer:
        server = cls(
            Gitlab('https://www.gitlab.com', private_token=private_token),
            allow_private,
            dashboard_maintainers_spec=dashboard_maintainers,
        )
        if private_token is not None:
            server.auth()
        server.register_project_spec(projects)
        return server

    @cached_property
    def registered_repositories(self) -> List[Repository]:
        return [
            repo
            for repo in self.__repo_by_id.values()
            if repo.registered and repo.visible
        ]

    def register_project_spec(self, spec: projects_spec.ProjectRef) -> None:
        if isinstance(spec, projects_spec.Project):
            repo = self.get_repo(spec)
            repo.register_config(spec)
        elif isinstance(spec, projects_spec.Group):
            group = self.get_group(spec)
            group.register_config(spec)
        elif isinstance(spec, projects_spec.List):
            for i in spec.items:
                self.register_project_spec(i.inherited_from(spec))
        else:
            raise ValueError(
                f'Unknown project spec kind {type(projects_spec)}'
            )

    def fetch_raw_project(
        self, id: projects_spec.Id
    ) -> gitlab_objects.Project:
        print(f'Fetching project {id}', flush=True)
        return self.projects.get(id)

    def fetch_raw_group(self, id: projects_spec.Id) -> gitlab_objects.Group:
        print(f'Fetching group {id}', flush=True)
        return self.groups.get(id)

    def get_repo(self, id: projects_spec.RefId) -> Repository:
        if isinstance(id, projects_spec.Ref):
            return self.get_repo(id.id)
        if isinstance(id, int):
            res = self.__repo_by_id.get(id)
        elif isinstance(id, str):
            res = self.__repo_by_name.get(id)
        else:
            raise ValueError(f'Unknown project ref kind {type(id)}')
        if res is None:
            res = self.__add_new_repo(self.fetch_raw_project(id))
        return res

    def get_group(self, id: projects_spec.RefId) -> Group:
        if isinstance(id, projects_spec.Ref):
            return self.get_group(id.id)
        if isinstance(id, int):
            res = self.__group_by_id.get(id)
        elif isinstance(id, str):
            res = self.__group_by_name.get(id)
        else:
            raise ValueError(f'Unknown group ref kind {type(id)}')
        if res is None:
            res = self.__add_new_group(self.fetch_raw_group(id))
        return res

    def __add_new_repo(
        self,
        gitlab_project: Union[
            gitlab_objects.Project, gitlab_objects.GroupProject
        ],
    ) -> Repository:
        res = Repository(self, gitlab_project)
        self.__repo_by_id[res.id] = res
        self.__repo_by_name[res.path_with_namespace] = res
        return res

    def get_or_add_raw_repo(
        self,
        gitlab_project: Union[
            gitlab_objects.Project, gitlab_objects.GroupProject
        ],
    ) -> Repository:
        res = self.__repo_by_id.get(gitlab_project.id)
        if res is None:
            res = self.__add_new_repo(gitlab_project)
        return res

    def __add_new_group(self, gitlab_group: gitlab_objects.Group) -> Group:
        res = Group(self, gitlab_group)
        self.__group_by_id[res.id] = res
        self.__group_by_name[res.full_path] = res
        return res

    def users_from_people_spec(
        self,
        spec: Optional[projects_spec.PeopleSpec],
        *,
        default: Union[
            Tuple[Set[User], str], Callable[[], Tuple[Set[User], str]]
        ],
        repo: Optional[Repository] = None,
    ) -> Tuple[Set[User], str]:
        if spec is None or isinstance(spec, projects_spec.Default):
            if callable(default):
                return default()
            else:
                return default
        elif isinstance(spec, projects_spec.Users):
            return (
                {
                    self.user_from_username(username)
                    for username in spec.usernames
                },
                'from hardcoded list',
            )
        elif isinstance(spec, projects_spec.Issue):
            issue = self.get_issue(spec, repo=repo)
            issue.is_special = True
            return (issue.assignees, f'from issue {issue.printable_id}')
        else:
            raise ValueError(f'Unknown people spec kind {type(spec)}')

    def get_issue(
        self, id: projects_spec.RefId, *, repo: Optional[Repository] = None
    ) -> Issue:
        if isinstance(id, projects_spec.Ref):
            return self.get_issue(id.id, repo=repo)
        if isinstance(id, int):
            if repo is None:
                raise ValueError(
                    f'Cannot get issue {id} with no specified repository'
                )
            else:
                return repo.get_issue(id)
        elif isinstance(id, str):
            reponame, issue_id = parse_issue_ref(id)
            return self.get_repo(reponame).get_issue(issue_id)
        else:
            raise ValueError(f'Unknown issue reference kind {type(id)}')

    def user_from_username(self, username: str) -> User:
        res = self.users.get(username)
        if res is None:
            res = User(self, username)
            self.users[username] = res
        return res

    def user_from_dict(self, dic: dict) -> User:
        return self.user_from_username(dic['username']).update_from_dict(dic)

    def user_from_project_member(
        self, mem: gitlab_objects.ProjectMember
    ) -> User:
        return self.user_from_username(
            mem.username
        ).update_from_project_member(mem)

    def __repr__(self) -> str:
        return f'{type(self).__name__}({self.wrapped.__repr__()})'

    @cached_property
    def dashboard_maintainers(self) -> Set[User]:
        return self.users_from_people_spec(
            self.dashboard_maintainers_spec, default=(set(), '')
        )[0]

    def resolve_deps(self) -> None:
        for repo in self.registered_repositories:
            repo.resolve_deps()
