from os import path
from functools import cached_property
from datetime import datetime
from typing import Dict, Iterable, List, Mapping, Optional, Tuple
from utils import Wrapper
import itertools
import os
import action
import jinja2
import xgitlab

# Doc for
# Jinja: https://jinja.palletsprojects.com/en/2.11.x/templates
# Bootstrap: https://getbootstrap.com/docs/5.0/


def by_user(
    actions: Iterable[action.Action],
    extra_users: Iterable[xgitlab.User] = [],
) -> Dict[xgitlab.User, List[action.Action]]:
    users = {action.who for action in actions} | set(extra_users)
    return {
        user: [action for action in actions if action.who == user]
        for user in users
    }


OptRc = Optional[Tuple[xgitlab.Resource, int]]
OptAc = Optional[Tuple[OptRc, action.Action, int]]
GroupedActions = Iterable[Tuple[OptAc, str, List[xgitlab.User]]]
SelRepoLabel = Optional[Tuple[xgitlab.Repository, Optional[xgitlab.Label]]]


def sort_actions(
    actions: Iterable[action.Action],
) -> GroupedActions:
    def group_rc_actions(
        rc: xgitlab.Resource, actions: List[action.Action]
    ) -> GroupedActions:
        grouped = [
            (
                actions[0],
                [
                    (why, [a.who for a in actions])
                    for why, actions in itertools.groupby(
                        actions, key=lambda action: action.why
                    )
                ],
            )
            for actions in (
                list(actions)
                for _uid, actions in itertools.groupby(
                    actions, key=lambda action: action.uid
                )
            )
        ]
        rowspan = sum(len(whys) for _a, whys in grouped)
        fst: OptRc = (rc, rowspan)
        for a, whys in grouped:
            snd: OptAc = (fst, a, len(whys))
            for why, whos in whys:
                yield (snd, why, whos)
                snd = None
            fst = None

    def weigh_rc(
        rc_actions: Tuple[xgitlab.Resource, Iterable[action.Action]],
    ) -> Tuple[List[action.TActionWeight], GroupedActions]:
        rc, actions = rc_actions
        sorted_by_weight = sorted(
            actions,
            key=lambda action: (
                action.weight,
                action.uid,
                action.why,
                action.who,
            ),
            reverse=True,
        )
        weights = [action.weight for action in sorted_by_weight]
        return (weights, group_rc_actions(rc, sorted_by_weight))

    def rc_key(action: action.Action) -> xgitlab.Resource:
        return action.resource

    grouped_by_rc = itertools.groupby(sorted(actions, key=rc_key), key=rc_key)
    weighted_rc = map(weigh_rc, grouped_by_rc)
    by_rc_weight = sorted(
        weighted_rc,
        reverse=True,
        key=lambda w_rc: w_rc[0],
    )
    for _weight, acs in by_rc_weight:
        yield from acs


class PrintableDatetime(Wrapper):
    def __init__(self):
        super().__init__(datetime.utcnow())

    @cached_property
    def printable(self) -> str:
        return self.wrapped.strftime('%Y-%m-%d %H:%M:%S UTC')

    @cached_property
    def js_parsable(self) -> str:
        return self.wrapped.strftime('%Y-%m-%dT%H:%M:%SZ')


class Printer:
    @cached_property
    def env(self) -> jinja2.environment.Environment:
        return jinja2.Environment(
            loader=jinja2.FileSystemLoader('templates'),
            autoescape=jinja2.select_autoescape(['html', 'xml']),
            undefined=jinja2.StrictUndefined,
        )

    @cached_property
    def template(self) -> jinja2.environment.Template:
        return self.env.get_template('actions.html')

    def print_index(
        self,
        actions: Iterable[action.Action],
        actions_by_user: Mapping[xgitlab.User, List[action.Action]],
        *,
        repositories: List[xgitlab.Repository],
        selected_repo_label: SelRepoLabel,
        output_dir: str,
        root: str,
    ) -> None:
        sorted_actions = list(sort_actions(actions))

        actions_counts = {
            user: len(actions)
            for user, actions in actions_by_user.items()
            if any(actions)
        }

        if selected_repo_label is None:
            nav: Tuple[List[Tuple[str, str]], str] = (
                [],
                'Tezos Developer Dashboard',
            )
        else:
            prev = [
                (
                    'Tezos Developer Dashboard',
                    path.normpath(path.join(root, 'index.html')),
                )
            ]
            if selected_repo_label[1] is None:
                nav = (prev, selected_repo_label[0].printable_id)
            else:
                prev += [
                    (selected_repo_label[0].printable_id, '../index.html')
                ]
                nav = (prev, selected_repo_label[1].name)

        with open(path.join(output_dir, 'index.html'), 'w') as output:
            output.write(
                self.template.render(
                    root=root,
                    nav=nav,
                    selected_repo_label=selected_repo_label,
                    repositories=repositories,
                    sorted_actions=sorted_actions,
                    actions_counts=actions_counts,
                    updated_on=PrintableDatetime(),
                )
            )

    def print_user_actions(
        self,
        user: xgitlab.User,
        user_actions: List[action.Action],
        *,
        selected_repo_label: SelRepoLabel,
        output_dir: str,
        root: str,
    ) -> None:
        sorted_actions = list(sort_actions(user_actions))

        repositories = set(a.resource.repo for a in user_actions)

        prev = [
            (
                'Tezos Developer Dashboard',
                path.normpath(path.join(root, 'index.html')),
            )
        ]
        if selected_repo_label is not None:
            if selected_repo_label[1] is None:
                prev += [(selected_repo_label[0].printable_id, 'index.html')]
            else:
                prev += [
                    (selected_repo_label[0].printable_id, '../index.html'),
                    (selected_repo_label[1].name, 'index.html'),
                ]
        nav = (prev, user.printable)

        with open(
            path.join(output_dir, f'{user.filename}.html'), 'w'
        ) as output:
            output.write(
                self.template.render(
                    root=root,
                    nav=nav,
                    selected_user=user,
                    selected_repo_label=selected_repo_label,
                    repositories=repositories,
                    sorted_actions=sorted_actions,
                    updated_on=PrintableDatetime(),
                )
            )

    def print_for_repo_label(
        self,
        server: xgitlab.Server,
        actions: List[action.Action],
        *,
        selected_repo_label: SelRepoLabel,
        output_dir: str,
        root: str,
    ) -> None:
        os.makedirs(output_dir, exist_ok=True)

        actions_by_user = by_user(actions, extra_users=server.users.values())

        repositories = (
            [selected_repo_label[0]]
            if selected_repo_label is not None
            else server.registered_repositories
        )
        self.print_index(
            actions,
            actions_by_user,
            repositories=repositories,
            selected_repo_label=selected_repo_label,
            output_dir=output_dir,
            root=root,
        )

        # For now, save space/time by avoiding repo/label/user combination
        # explosion
        if (
            selected_repo_label is not None
            and selected_repo_label[1] is not None
        ):
            return

        for user, user_actions in actions_by_user.items():
            self.print_user_actions(
                user,
                user_actions,
                selected_repo_label=selected_repo_label,
                output_dir=output_dir,
                root=root,
            )

    def print_for_repo(
        self,
        server: xgitlab.Server,
        actions: List[action.Action],
        *,
        selected_repo: xgitlab.Repository,
        output_dir: str,
        root: str,
    ) -> None:

        self.print_for_repo_label(
            server,
            actions,
            selected_repo_label=(selected_repo, None),
            output_dir=output_dir,
            root=root,
        )

        for lbl in selected_repo.all_labels:
            dir = path.join(output_dir, lbl.filename)
            root_for_label = path.join(root, '..')
            actions_for_label = [
                a for a in actions if lbl in a.resource.labels
            ]
            self.print_for_repo_label(
                server,
                actions_for_label,
                selected_repo_label=(selected_repo, lbl),
                output_dir=dir,
                root=root_for_label,
            )

    def print_all(
        self,
        server: xgitlab.Server,
        actions: List[action.Action],
        *,
        output_dir: str,
    ) -> None:

        self.print_for_repo_label(
            server,
            actions,
            selected_repo_label=None,
            output_dir=output_dir,
            root='.',
        )

        for repo in server.registered_repositories:
            dir = path.join(output_dir, repo.filepath)
            root = path.relpath(output_dir, start=dir)
            actions_in_repo = [a for a in actions if a.resource.repo is repo]
            self.print_for_repo(
                server,
                actions_in_repo,
                selected_repo=repo,
                output_dir=dir,
                root=root,
            )
