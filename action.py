from abc import ABC
from functools import cached_property
from typing import Any, Tuple
from utils import GitlabDatetime
import xgitlab

TActionBaseWeight = int
TActionPostWeight = Any  # Any comparable type
TActionWeight = Tuple[
    xgitlab.TResourcePreWeight,
    TActionBaseWeight,
    TActionPostWeight,
    xgitlab.TResourcePostWeight,
]


class Action(ABC):
    base_weight: TActionBaseWeight
    base_assignee_weight: TActionBaseWeight

    def __init__(
        self,
        resource: xgitlab.Resource,
        who: xgitlab.User,
        *,
        why: str,
        extra_uid: str = '',
        post_weight: TActionPostWeight = (),
    ):
        self.resource = resource
        # who is responsible for performing the action
        self.who = who
        self.why = why
        self.post_weight = post_weight
        self.uid = f'{resource.uid}.{type(self).__name__}'
        if extra_uid != '':
            self.uid += f'.{extra_uid}'

    @cached_property
    def web_url(self) -> str:
        return self.resource.web_url

    @cached_property
    def weight(self) -> TActionWeight:
        is_assigned = self.who in self.resource.assignees
        return (
            self.resource.pre_weight,
            self.base_weight + is_assigned * self.base_assignee_weight,
            self.post_weight,
            self.resource.post_weight,
        )


class MRAction(Action, ABC):
    base_assignee_weight = 1001

    def __init__(
        self,
        mr: xgitlab.MergeRequest,
        who: xgitlab.User,
        **kw,
    ):
        super().__init__(mr, who, **kw)


class IssueAction(Action, ABC):
    base_assignee_weight = 501

    def __init__(
        self,
        issue: xgitlab.Issue,
        who: xgitlab.User,
        **kw,
    ):
        super().__init__(issue, who, **kw)


class Merge(MRAction):
    printable = 'Merge'
    description = (
        'MR is ready to be merged and you have merge power'
        '(assign to marge-bot)'
    )
    base_weight = 2100

    def __init__(self, mr: xgitlab.MergeRequest, who: xgitlab.User, why: str):
        super().__init__(
            mr,
            who,
            why=why,
            extra_uid=mr.base_uid,
        )


class Rebase(MRAction):
    printable = 'Rebase'
    description = 'MR is ready but needs automatic rebase before being merged'
    base_weight = 2000

    def __init__(self, mr: xgitlab.MergeRequest, who: xgitlab.User, why: str):
        super().__init__(
            mr,
            who,
            why=why,
            extra_uid=mr.base_uid,
        )


class ManualRebase(MRAction):
    printable = 'Rebase manually'
    description = 'MR is ready but needs manual rebase'
    base_weight = 1900

    def __init__(self, mr: xgitlab.MergeRequest, who: xgitlab.User, why: str):
        super().__init__(
            mr,
            who,
            why=why,
            extra_uid=mr.base_uid,
        )


class ActAssignee(MRAction):
    printable = 'Act'
    description = 'You are assigned'
    base_weight = 1850

    def __init__(self, mr: xgitlab.MergeRequest, who: xgitlab.User):
        super().__init__(
            mr,
            who,
            why='is assigned',
        )


class MRCommentAction(MRAction, ABC):
    def __init__(
        self,
        mr: xgitlab.MergeRequest,
        who: xgitlab.User,
        why: str,
        comment: xgitlab.Comment,
    ):
        self.comment = comment
        super().__init__(
            mr,
            who,
            post_weight=[comment.weight],
            why=why,
            extra_uid=comment.uid,
        )

    @cached_property
    def web_url(self) -> str:
        return self.comment.web_url


class CloseThreadOrReply(MRCommentAction):
    printable = 'Close or reply to thread'
    description = (
        "The author has responded to a thread in which "
        "you've participated, please reply or close the "
        "thread"
    )
    base_weight = 1800

    def __init__(
        self,
        mr: xgitlab.MergeRequest,
        who: xgitlab.User,
        comment: xgitlab.Comment,
    ):
        super().__init__(mr, who, why='is the thread author', comment=comment)


class ReplyToThread(MRCommentAction):
    printable = 'Reply to thread'
    description = (
        "A thread is open on your MR to which you've not "
        "replied, please reply."
    )
    base_weight = 1700

    # inherited init


class ReviewOrApprove(MRAction):
    printable = 'Review or approve'
    description = (
        "This MR requires a review or an approve, and "
        "you're assigned as a reviewer"
    )
    base_weight = 1600

    def __init__(self, mr: xgitlab.MergeRequest, who: xgitlab.User, why: str):
        super().__init__(mr, who, why=why)


class FixCI(MRAction):
    printable = 'Fix CI'
    description = 'The latest pipeline of this MR failed, please fix'
    base_weight = 1500

    def __init__(
        self,
        mr: xgitlab.MergeRequest,
        who: xgitlab.User,
        why: str,
        pipeline: dict,
    ):
        self.pipeline = pipeline
        post_weight = [
            -GitlabDatetime.parse(pipeline['updated_at']).timestamp()
        ]
        super().__init__(
            mr,
            who,
            post_weight=post_weight,
            why=why,
            extra_uid=pipeline['id'],
        )

    @cached_property
    def web_url(self) -> str:
        return self.pipeline['web_url']


class CompleteMRTask(MRAction):
    printable = 'Complete task'
    description = 'This MR has uncompleted tasks'
    base_weight = 1400

    def __init__(self, mr: xgitlab.MergeRequest, who: xgitlab.User, why: str):
        super().__init__(
            mr,
            who,
            post_weight=[mr.task_completion_weight],
            why=why,
            extra_uid=mr.task_completion_uid,
        )


class AddMTReviewers(MRAction):
    printable = 'Add a merge-team reviewer'
    base_weight = 1300

    def __init__(self, mr: xgitlab.MergeRequest, who: xgitlab.User, why: str):
        super().__init__(mr, who, why=why)
        # suggest MT members?
        self.description = (
            'This MR lacks merge-team reviewers, '
            'please add one or refer to '
            'merge-dispatchers '
            f'{mr.repo.printable_dispatchers}.'
        )


class AddReviewers(MRAction):
    printable = 'Add reviewers'
    base_weight = 1200

    def __init__(self, mr: xgitlab.MergeRequest, who: xgitlab.User, why: str):
        super().__init__(mr, who, why=why)
        # suggest reviewers?
        self.description = (
            'This MR lacks reviewers, please add one '
            'or refer to merge-dispatchers '
            f'{mr.repo.printable_dispatchers}.'
        )


class AssignIssue(IssueAction):
    printable = 'Assign issue'
    description = 'This issue is open and unassigned'
    base_weight = 1100

    def __init__(self, issue: xgitlab.Issue, who: xgitlab.User, why: str):
        super().__init__(issue, who, why=why)


class HelpAddMTReviewers(MRAction):
    printable = 'Help add merge-team reviewers'
    description = 'This MR lacks merge-team reviewers, please help to add one'
    base_weight = 1000

    def __init__(self, mr: xgitlab.MergeRequest, who: xgitlab.User):
        super().__init__(
            mr,
            who,
            why='is a dispatcher',
        )


class HelpAddReviewers(MRAction):
    printable = 'Help add reviewers'
    description = 'This MR lacks reviewers, please help to add one'
    base_weight = 900

    def __init__(self, mr: xgitlab.MergeRequest, who: xgitlab.User):
        super().__init__(
            mr,
            who,
            why='is a dispatcher',
        )


class UnWIP(MRAction):
    printable = 'UnWIP'
    description = (
        'This MR is in WIP state. To move it forward, mark'
        ' as ready if applicable'
    )
    base_weight = 800
    base_assignee_weight = 0

    def __init__(self, mr: xgitlab.MergeRequest, who: xgitlab.User, why: str):
        super().__init__(mr, who, why=why)


class CompleteIssueTask(IssueAction):
    printable = 'Complete task'
    description = 'This issue has uncompleted tasks'
    base_weight = 700

    def __init__(self, issue: xgitlab.Issue, who: xgitlab.User, why: str):
        super().__init__(
            issue,
            who,
            post_weight=[issue.task_completion_weight],
            why=why,
            extra_uid=issue.task_completion_uid,
        )


class SolveIssue(IssueAction):
    printable = 'Solve issue'
    description = 'This issue is open'
    base_weight = 600

    def __init__(self, issue: xgitlab.Issue, who: xgitlab.User, why: str):
        super().__init__(issue, who, why=why)


class UnknownAction(MRAction):
    printable = 'No idea what to do'
    description = 'Action collector could not determine an action'
    base_weight = 500

    def __init__(self, mr: xgitlab.MergeRequest, who: xgitlab.User):
        self.base_weight, why = (
            (2300, 'is a dashboard maintainer')
            if who.is_dashboard_maintainer
            else (500, 'is a dispatcher')
        )
        super().__init__(mr, who, why=why)


class UnblockMR(MRAction):
    printable = 'Unblock'
    description = 'This MR is blocked'
    base_weight = 400

    def __init__(self, mr: xgitlab.MergeRequest, who: xgitlab.User, why: str):
        super().__init__(mr, who, why=why)
        self.description = f'This MR is blocked by {mr.printable_blockers}'


class UnblockIssue(IssueAction):
    printable = 'Unblock'
    description = 'This issue is blocked'
    base_weight = 300

    def __init__(self, issue: xgitlab.Issue, who: xgitlab.User, why: str):
        super().__init__(issue, who, why=why)
        self.description = (
            f'This issue is blocked by {issue.printable_blockers}'
        )


class CompleteMetaIssueTask(IssueAction):
    printable = 'Meta issue task'
    description = 'This meta-issue has uncompleted tasks'
    base_weight = 250

    def __init__(self, issue: xgitlab.Issue, who: xgitlab.User, why: str):
        super().__init__(
            issue,
            who,
            post_weight=[issue.task_completion_weight],
            why=why,
            extra_uid=issue.task_completion_uid,
        )


class WorkOnMetaIssue(IssueAction):
    printable = 'Meta issue'
    description = 'This meta-issue may require action'
    base_weight = 200


class WaitForPipeline(MRAction):
    printable = 'Wait for pipeline'
    description = 'Pipeline is running'
    base_weight = 100

    def __init__(self, mr: xgitlab.MergeRequest, who: xgitlab.User, why: str):
        super().__init__(mr, who, why=why)
