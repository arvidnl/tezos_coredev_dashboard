from typing import (
    Any,
    Callable,
    Dict,
    List,
    Optional,
    Union,
    TypeVar,
)
from typing_extensions import Protocol
from functools import cached_property, partial
from datetime import datetime, timedelta
from os import path as os_path
import json
import os
import gitlab  # type: ignore
import utils

ROOT = '_cache_v1'

DEFAULT_MAX_AGE = timedelta(minutes=1)


class WrappedParent(Protocol):
    @cached_property
    def filepath(self) -> str:
        ...

    wrapped: gitlab.base.RESTObject


def do_list(f, x):
    return list(map(f, x))


Jsonish = Union[Dict[str, Any], List[Dict[str, Any]]]


def get_attributes(x) -> Jsonish:
    return x.attributes


def add_to_cache(
    full_path: str,
    raw: Jsonish,
) -> None:
    os.makedirs(os_path.dirname(full_path), exist_ok=True)
    with open(full_path, 'w') as output:
        output.write(f'{datetime.utcnow().isoformat()}\n')
        json.dump(raw, output)


TObj = TypeVar('TObj')


def add(wrapped_parent: WrappedParent, subpath: List[Any], obj: TObj) -> TObj:
    full_path = (
        os_path.join(ROOT, wrapped_parent.filepath, *map(str, subpath))
        + '.json'
    )
    add_to_cache(full_path, get_attributes(obj))
    return obj


def fetch(
    what: str,
    *,
    path: List[str],
    f: Callable,
    of_raw: Callable[[Jsonish], object] = lambda x: x,
    to_raw: Callable[[Any], Jsonish] = lambda x: x,
    args: List[Any] = [],
    keywords: Dict[str, Any] = {},
    after: Optional[datetime] = None,
    max_age: timedelta = DEFAULT_MAX_AGE,
):
    full_path = os_path.join(ROOT, *path, *map(str, args)) + '.json'
    if os_path.isfile(full_path):
        with open(full_path, 'r') as input:
            last_update = datetime.fromisoformat(input.readline().rstrip('\n'))
            cached = json.load(input)
        if last_update + max_age >= (after or utils.START):
            print(f'  Got {what} from cache', flush=True)
            return of_raw(cached)
        aft_str = after or f'start={utils.START}'
        reason = f'cache outdated ({last_update} < {aft_str})'
    else:
        reason = 'cache miss'
    print(f'  Fetching {what} ({reason})', flush=True)
    res = f(*args, **keywords)
    add_to_cache(full_path, to_raw(res))
    return res


def get_one(
    cls,
    what: str,
    wrapped_parent: WrappedParent,
    getter_path: List[str],
    **kw,
):
    manager = wrapped_parent
    for elt in getter_path:
        manager = getattr(manager, elt)
    return fetch(
        what,
        path=[wrapped_parent.filepath] + getter_path,
        f=getattr(manager, 'get'),
        of_raw=partial(cls, manager),
        to_raw=get_attributes,
        **kw,
    )


def get_list(
    cls,
    what: str,
    wrapped_parent: WrappedParent,
    getter_path: List[str],
    **kw,
):
    manager = wrapped_parent
    for elt in getter_path:
        manager = getattr(manager, elt)
    return fetch(
        what,
        path=[wrapped_parent.filepath] + getter_path,
        f=getattr(manager, 'list'),
        of_raw=partial(do_list, partial(cls, manager)),
        to_raw=partial(do_list, get_attributes),
        **kw,
    )
