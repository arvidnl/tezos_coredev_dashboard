from datetime import datetime

START = datetime.utcnow()


class Wrapper:
    def __init__(self, wrapped):
        self.wrapped = wrapped

    def get_wrapped_attr(self, attr):
        try:
            return getattr(self.wrapped, attr)
        except AttributeError as error:
            if attr == 'wrapped_more':
                raise error
            more = getattr(self, 'wrapped_more', None)
            if more is None:
                raise error
            else:
                self.wrapped = more
                return getattr(more, attr)

    def __getattr__(self, attr):
        return self.get_wrapped_attr(attr)


class GitlabDatetime:
    @staticmethod
    def parse(text: str) -> datetime:
        if text.endswith('Z'):
            text = text[:-1]
        return datetime.fromisoformat(text)

    @staticmethod
    def of_dt(dt: datetime) -> str:
        text = dt.isoformat()
        return text
        # Below: what would be needed to fit exactly gitlab format, not
        #        needed for now
        # point_pos = text.find('.')
        # if point_pos < 0:
        #     return f'{text}.000Z'
        # text += '000'
        # return f'{text[:point_pos+4]}Z'

    @classmethod
    def of_now(cls) -> str:
        return cls.of_dt(datetime.utcnow())
