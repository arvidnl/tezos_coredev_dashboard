from projects_spec import *

DASHBOARD_MAINTAINERS = Users('mbouaziz')

PROJECTS = List(
    Group('tezos'),
    Project(
        'tezos/tezos',
        dispatchers=Issue('tezos/tezos#1062'),
        issuewatchers=Issue('tezos/tezos#1061'),
        pre_weight=8,
    ),
    Project('tezos/opam-repository', dispatchers=Users('pirbo'), pre_weight=8),
    Project('oxheadalpha/merbocop', pre_weight=7),
    Project('arvidnl/tezos_coredev_dashboard', pre_weight=6),
    Project('arvidnl/tezos_merge_team_scripts', pre_weight=5),
    Project('arvidnl/tezos-merge-team-scripts'),
    Group('nomadic-labs'),
    Project('MBourgoin/issuebot'),
    Project('metastatedev/tezos', pre_weight=-2),
)
